import gulp from 'gulp';
import sass from 'gulp-sass';
import wait from 'gulp-wait';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean-css';
import browserSync from 'browser-sync';
import del from 'del';

const sync = browserSync.create();
const reload = sync.reload;
const config = {
  paths: {
    src: {
      html: './src/**/*.html',
      img: './src/img/**/*.*',
      sass: ['./src/sass/**/*.scss']
    },
    dist: {
      main: './dist',
      css: './dist/css',
      img: './dist/img'
    }
  }
};

gulp.task('sass', gulp.series(
  function moveSass(){
    return gulp.src(config.paths.src.sass)
      .pipe(wait(100))
      .pipe(sass())
      .pipe(autoprefixer({
          browsers: ['last 2 versions']
      }))
      .pipe(clean())
      .pipe(gulp.dest(config.paths.dist.css))
      .pipe(sync.stream());
  },
  refresh
));

function refresh(done){
  reload();
  done();
}

gulp.task('static', gulp.series(
  function moveHtml(){
    return gulp.src(config.paths.src.html)
      .pipe(gulp.dest(config.paths.dist.main));
  },
  function moveImages(){
    return gulp.src(config.paths.src.img)
      .pipe(gulp.dest(config.paths.dist.img));
  },
  refresh
));

gulp.task('clean', () => {
  return del([config.paths.dist.main]);
});

gulp.task('build', gulp.series(['clean', 'sass', 'static']));

function server() {
  sync.init({
    injectChanges: true,
    server: config.paths.dist.main
  });
};

gulp.task('default', gulp.series(['build']));

gulp.task('watch', gulp.series(['default'], function watch() {
  gulp.watch('src/sass/*.scss', gulp.series(['sass']));
  gulp.watch('src/*.html', gulp.series(['static']));
  gulp.watch('src/img/**/*.*', gulp.series(['static']));
  return server();
}));